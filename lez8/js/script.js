function inserisciPersona(){
    var varNome = document.getElementById("input_nome").value;
    var varCognome = document.getElementById("input_cognome").value;
    var varNascita = document.getElementById("input_nascita").value;

    var persona = {
        nome: varNome,
        cognome: varCognome,
        nascita: varNascita
    };

    elencoPersone.push(persona);
    stampaArray();
}

function stampaArray(){
    var stringaOutput = "";

    for(var i=0; i<elencoPersone.length; i++){
        stringaOutput += "<tr>"; 
        stringaOutput += "  <td>" + elencoPersone[i].nome + "</td>";
        stringaOutput += "  <td>" + elencoPersone[i].cognome + "</td>";
        stringaOutput += "  <td>" + elencoPersone[i].nascita + "</td>";
        stringaOutput += "</tr>";
    }

    document.getElementById("contenuto-tabella").innerHTML = stringaOutput;
}

//Main
var elencoPersone = [];