function aggiornaTabella(){
    $.ajax({
        url: "http://localhost:8085/prodotto/",
        type: "GET",
        dataType: "json",
        success: function(risultato){
            stampaArray(risultato);
        },
        error: function(errore){
            console.log("Sono nel ramo di Error");
            console.log(errore)
        }
    })
}

function stampaArray(varRisultato){
    var outTabella = "";
    for(var i=0; i<varRisultato.length; i++){
        
        outTabella += "<tr>";
        outTabella += " <td>" + varRisultato[i].id + "</td>";
        outTabella += " <td>" + varRisultato[i].nome + "</td>";
        outTabella += " <td>" + varRisultato[i].codice + "</td>";
        outTabella += " <td>" + varRisultato[i].prezzo + "</td>";
        outTabella += " <td><button type='button' class='btn btn-danger' onclick='eliminaProdotto(" + varRisultato[i].id + ")'>Cancella</button></td>";
        outTabella += " <td><button type='button' class='btn btn-warning' onclick='modaleModificaProdotto(" + varRisultato[i].id + ")'>Modifica</button></td>";
        outTabella += "</tr>";

    }

    document.getElementById("contenuto-tabella").innerHTML = outTabella;
}

function inserisciProdotto(){
    var prodNome = document.getElementById("input_nome").value;
    var prodCodice = document.getElementById("input_codice").value;
    var prodPrezzo = document.getElementById("input_prezzo").value;

    var prodotto = {
        nome: prodNome,
        codice: prodCodice,
        prezzo: prodPrezzo
    }

    $.ajax({
        url: "http://localhost:8085/prodotto/inserisci",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(prodotto),
        dataType: "json",                       //In che modo interpreto il risultato della response
        success: function(risultato){
            if(risultato.id){
                alert("Prodotto inserito")
                aggiornaTabella()
            }
            else{
                alert("Errore di inserimento")
            }
        },
        error: function(errore){
            console.log(errore)
        }
    })
}

function eliminaProdotto(varId){
    if(varId){

        $.ajax({
            // url: "http://localhost:8085/prodotto/" + varId,
            url: `http://localhost:8085/prodotto/${varId}`,         //Specifica ECMASCRIPT
            type: "DELETE",
            dataType: "json",
            success: function(risultato){
                if(risultato){
                    alert("Operazione effettuata con successo!");
                    aggiornaTabella();
                }
                else{
                    alert("Errore di eliminazione");
                }
            },
            error: function(errore){
                alert("Errore di eliminazione");
                console.log(errore)
            }
        })

    }
}

function modaleModificaProdotto(varId){

    $.ajax({
        url: "http://localhost:8085/prodotto/" + varId,
        type: "GET",
        dataType: "json",
        success: function(risultato){
            console.log(risultato);

            // document.getElementById("update_nome").value = risultato.nome;
            // document.getElementById("update_codice").value = risultato.codice;
            // document.getElementById("update_prezzo").value = risultato.prezzo;

            $("#update_nome").val(risultato.nome);
            $("#update_codice").val(risultato.codice);
            $("#update_prezzo").val(risultato.prezzo);

            $("#pulsanteModifica").data("prodottoid", risultato.id);        //SET del Data

            $("#modaleModifica").modal("toggle");
        },
        error: function(errore){
            alert("Errore di recupero informazioni");
            console.log(errore)
        }
    })

}

function modificaProdotto(varId){
    var varNome = $("#update_nome").val();
    var varCodice = $("#update_codice").val();
    var varPrezzo = $("#update_prezzo").val();

    var prodotto = {
        id: varId,
        nome: varNome,
        codice: varCodice,
        prezzo: varPrezzo
    }

    $.ajax({
        url: "http://localhost:8085/prodotto/modifica",
        type: "PUT",
        contentType: "application/json",
        data: JSON.stringify(prodotto),
        dataType: "json",
        success: function(risultato){
            if(risultato){
                alert("Operazione effettuata con successo");
                $("#modaleModifica").modal("toggle");
                aggiornaTabella();
            }
        },
        error: function(errore){
            alert("Errore di modifica informazioni");
            console.log(errore)
        }
    })
}

$(document).ready(
    function(){

        aggiornaTabella();

        $("#pulsanteModifica").click(
            function(){
                var prodId = $(this).data("prodottoid");
                
                modificaProdotto(prodId);
            }
        )

    }
)