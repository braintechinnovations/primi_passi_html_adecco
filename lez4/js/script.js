// var indice = 0;
// while(indice < 5){
//     console.log("CIAO GIO!");
    
//     indice++;
// }

// var eta = 23;
// if(eta >= 18){
//     console.log("Sei maggiorenne");
// }
// else{
//     console.log("Sei minorenne");
// }

//Elenco semplice
// var elenco = ["BMW", "FIAT", "Maserati"];

// for(var i=0; i<elenco.length; i++){
//     console.log(elenco[i])
// }

//Elenco eterogeneo
// var elencoEterogeneo = ["Bmw", "Gatto", 87, 589.2];

// for(var i=0; i<elencoEterogeneo.length; i++){
//     console.log(elencoEterogeneo[i])
// }

//Array di Array (matrice Eterogenea)
// var elencoStudenti = [
//     ["Giovanni", "Pace", 34, "CAFFE"],
//     ["Mario", "Rossi", 54],
//     ["Valeria", "Verdi"]
// ];

// for(var i=0; i<elencoStudenti.length; i++){
//     console.log(elencoStudenti[i]);
// }

////////////////JSON/////////////////////

var giovanni = {
    "nome": "Giovanni",
    "cognome": "Pace",
    "eta": 34,
    "animali": [ "gatto", "cane" ],
    "mezzi": [
        {
            "tipo": "moto",
            "cilindrata": 750
        },
        {
            "tipo": "auto",
            "capienza": 5
        }
    ]
}

console.log(giovanni.eta)
console.log(giovanni.mezzi[0].tipo)